Hi there 👋, I'm Iqboljon Abdumalikov

CONTACT
  - Email: happinessjonabdumalikov@gmail.com,
  - Phone number: +998907705702     
TECHNICAL SKILLS
  - HTML/CSS, Javascript, SCSS.
 
EDUCATION
  - MayoqHub IT academy based on self-study with mentor
    - Frontend development
      I learned frontend development from zero to advanced with the help of senior
      developer who gave me feedbacks for my projects developed during my
      studies.
    - I’m currently learning React Framework.
LANGUAGE
  - English
  - Russian (a little).

INTERESTS
  - Big fan of football
  - Crazy about playing DotA 1(Defence of the Ancients).
